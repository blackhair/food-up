module.exports = function(db, cb) {

	var cart = db.define("cart", {
		id 		: Number,
		coockie : Number,
		foods	: String
	});

	var order = db.define("orders", {
		id	: Number,
		status : String,
		foodType : String,
		restaurant : Number
	});

	var user = db.define("users", {
		id		: Number,
		name 	: String,
		tell 	: Number,
		address : String,
		orders  : String
	});

	var foods = db.define("foods", {
		id : Number,
		name : String,
		pic	 : String,
		price: String,
		desc: String,
		restaurant:String
	});

};