$(document).ready(function(){
	$('.dltFood').bind('click',function(e){
		e.preventDefault();
		if (confirm('are you sure ?')) {
			var id = $(this).attr('foodId');
			$.post('/deleteFood', {id : id} , function(data) {
				location.reload();
			});
		}
	});
	$('.modifyFood').click(function(){
		var id 		= $(this).attr('foodId');
		var price 	= $(this).parent().parent().find('.price').val();
		var stock 	= $(this).parent().parent().find('.stock').val();
		var off 	= $(this).parent().parent().find('.off').val();
		
		if(off.length > 0 && stock.length > 0 && price.length > 0) {
			$.post('/modifyFood', {id : id, stock: stock, price: price, off : off } , function(data) {
				location.reload();
			});
		}else{		
			$('.modal-body p').html("لطفا برای تغییر اطلاعات تمامی فیلد ها را پر کنید .");
			$('#myModal').modal('show');
		}
	});
});