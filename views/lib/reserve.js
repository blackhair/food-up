$(document).ready(function(){
    var foodsInfo      = [],
        eachFood       = {},
	    cookieArray = $.cookie('foodsId');



    $('.foodsBoxCart').each(function() {
        var price = +($(this).find('.priceSpanTag span').text());
        var totalPrice      = +($('.totalPrice').text());
        var off = $(this).find('#rest').attr('off');
        if(off > 0 ){
            off = (off/100)*price;
            price = price-off;

        }
        $('.totalPrice').text(totalPrice+price);
    });

    $('.MMqty').click(function() {
        var numOfFood = +($(this).parent().find('.numOfFood').text());
        var price = +($(this).parent().parent().find('.priceSpanTag span').text());
        var totalPrice      = +($('.totalPrice').text());
        var off = $(this).parent().parent().find('#rest').attr('off');

        if (numOfFood > 1 ) {
            numOfFood--;
            $(this).parent().find('.numOfFood').text(numOfFood);
            if(off > 0 ){
                off = (off/100)*price;
                price = price-off;

            }            
            $('.totalPrice').text(totalPrice-price);
        }
    });

    $('.PPqty').click(function() {
        var numOfFood = +($(this).parent().find('.numOfFood').text());
        var stock = +($(this).parent().parent().find('.qtyForEachRest span').text());
        var price = +($(this).parent().parent().find('.priceSpanTag span').text());
        var totalPrice      = +($('.totalPrice').text());
        var off = $(this).parent().parent().find('#rest').attr('off');

        if (numOfFood < stock){
            numOfFood++;
            $(this).parent().find('.numOfFood').text(numOfFood);
            if(off > 0 ){
                off = (off/100)*price;
                price = price-off;

            }
            $('.totalPrice').text(totalPrice + price);
        }
    });

    $('.deleteFoodFromCart').click(function(){
        var numOfFood = +($(this).parent().parent().find('.numOfFood').text());
        var price = +($(this).parent().parent().find('.priceSpanTag span').text());
        var totalPrice      = +($('.totalPrice').text());
        var off = $(this).parent().parent().find('#rest').attr('off');
        $(this).parent().parent().parent().remove();

        if(totalPrice > 0){
            if(off > 0 ){
                off = (off/100)*price;
                price = price-off;

            }    
            var result = totalPrice - (numOfFood*price);
            $('.totalPrice').html(result);              
        }
    });
    
    var orderDetails, currentDay, userDateEntered, dateDatapicker;
    var date = moment().format('jD/jM/jYYYY');
    
    $("#date").datepicker({
        dateFormat: "yy/mm/dd",
        changeMonth: true,
        minDate: 1,
        startDate: "+2d",
        isRTL: true,
        maxDate: "+100D",
        defaultViewDate: "today +1 ",
        keepEmptyValues: false,
        onSelect: function(dateText, inst) {
            dateDatapicker = $(this).datepicker('getDate'); 
        }
    });
    currentDay = moment().jDate();
    
    $('.contiuneBuy').click(function() {
        if(dateDatapicker){
            $('.foodsBoxCart').each(function() {
                eachFood.name       = $(this).find('.foodName').text();
                eachFood.restId     = $(this).find('#rest').attr('restId');
                eachFood.restName   = $(this).find('#rest').text().replace(/\s{2,}/g, ' ');
                eachFood.qty        = $(this).find('.numOfFood').text();
                foodsInfo.push(eachFood);
                eachFood = {};        
            }).promise().done(function() {
                var totalPrice = +($('.totalPrice').text()); 
                var date       = $('#date').val();

                $.cookie('foodsInfo', foodsInfo);
                $.cookie('totalPrice', totalPrice);
                $.cookie('date', date);
                window.location = "/cart_login"
            });
        }else{
            $('.modal-body p').html("لطفا تاریخ رزرو خود را مشخص کنید .");
            $('#myModal').modal('show');
        }
    });
});	