$(document).ready(function(){
	$('.loginBtn').click(function() {
		var username , password;
		var checkLengthUser = $('.userNameLogin').val();
		var checkLengthPass = $('.passwordLogin').val();
		if( checkLengthPass.length > 6 && checkLengthUser.length == 11 && checkLengthUser.indexOf("09") != -1){
			password = checkLengthPass;
			username = checkLengthUser;

			$.post('/loginUser', { user: username , pass: password}, function(data) {
				if(data.status == "200"){
					$.cookie('userInfoForPay', data.userid);
					window.location.href = "./pay";
				}else{
	                $('.modal-body p').html("نام کاربری یا رمز عبور اشتباه است");
                	$('#myModal').modal('show');
				}
			});
		}else{
			$('.modal-body p').html("در وارد کردن اطلاعات دقت کنید . ");
        	$('#myModal').modal('show');
		}
	});
	$('.signupBtn').click(function() {
		var username, password, address, name;	
		var checkLengthUser = $('.telSignUp').val();
		var checkLengthPass = $('.passSignUp').val();		
		if( checkLengthPass.length > 6 && checkLengthUser.length == 11 && checkLengthUser.indexOf("09") != -1){
			password = checkLengthPass;
			username = checkLengthUser;
			address  = $('.addressSignUp').val();
			name 	 = $('.nameSignUp').val();
			$.post('/signUpUser', {name: name, user: username , pass: password, address : address}, function(data) {
				if(data.status == "200"){
					$.cookie('userInfoForPay', data.userid);
					window.location.href = "./pay";
				}else{
					$('.modal-body p').html("این شماره تلفن قبلا ثبت شده است .");
    				$('#myModal').modal('show');
				}
			});
		}else{
			$('.modal-body p').html("- در وارد کردن تلفن همراه خود دقت کنید .<br> - رمز عبور شما باید از ۶ حرف بیشتر باشد .");
			$('#myModal').modal('show');
		}
	});
});