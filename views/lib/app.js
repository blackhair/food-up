$(document).ready(function() {
	$('.nav-link').click(function(){
		if($( window ).width() < 991) {
	    	$('html, body').animate({
	        	scrollTop: $( $(this).attr('href') ).offset().top - 2
	    	}, 1000,function () {
	        	window.location.hash = "";
	    	});
		}if($( window ).width() > 991) {
	    	$('html, body').animate({
	        	scrollTop: $( $(this).attr('href') ).offset().top - 2
	    	}, 1000,function () {
	        	window.location.hash = "";
	    	});
		}	
	});

	$('.header-search').keyup(function() {
		var text = $('.header-search').val();
		if(text.length >= 3) {
			$('.gifLoading').css({"opacity":"1"});
			$.post('/searchFood', {name : text}, function(data) {
				if(data.foods.length > 0){
					$('.restaurantMainBox').css({"z-index": "-1"});
					$('.searchResult').css({"height": "auto", "transition" : "all 200ms"});
					$('.searchResult').html(" ");
					$.each(data.foods, function(key, food) {

						$('.searchResult').append('<a class="moi" href="#'+ food._id +'"><li class="itemSearchResult"><img class="imgInItemSearch" src="'+food.pic+'"><span>'+food.name+'</span>'+food.desc+'</li></a>');
						$('.gifLoading').css({"opacity":"0"});
					});
						$('.itemSearchResult').click(function(){
							if($( window ).width() < 991) {
						    	$('html, body').animate({
						        	scrollTop: $( $(this).parent().attr('href') ).offset().top - 45
						    	}, 1000,function () {
						        	window.location.hash = "";
						    	});
							}if($( window ).width() > 991) {
						    	$('html, body').animate({
						        	scrollTop: $( $(this).parent().attr('href') ).offset().top - 10
						    	}, 1000,function () {
						        	window.location.hash = "";
						    	});
							}					       
						});	
				}else{
					$('.restaurantMainBox').css({"z-index": "1"});
					$('.searchResult').css({"height": "auto", "transition" : "all 200ms"});
					$('.searchResult').html(" ");
					$('.searchResult').append('<li class="itemSearchResult"><span style="padding-right:8px">متاسفانه همچین غذایی پیدا نشد  :( <span></li>');
					$('.gifLoading').css({"opacity":"0"});
				}
			});
		}else{
			$('.searchResult').html(" ");
			$('.restaurantMainBox').css({"z-index": "1"});
		}
	});
	$('.flwUpBtn').click(function() {
		var flwUpNum = $('.flwUp').val();
		$.post('/flwUpOrder', {flwUpNum : flwUpNum}, function(data) {
			
			$('.modal-title').html('اطلاعات سفارش');
			if(data.status == true){
				$('.modal-body p').html("سفارش شما از رستوران '" + data.rest_name + "' در تاریخ '" +data.date +  "' به آدرس '" + data.address + "' ارسال شد. ");
			}else{$('.modal-body p').html("سفارش شما از رستوران '" + data.rest_name + "' در تاریخ '" +data.date +  "' به آدرس '" + data.address + "' ارسال نشد. ");}
			$('#myModal').modal('show');

		});
	});

  $("#menu-close").click(function(e) {
    e.preventDefault();
    $("#sidebar-wrapper").toggleClass("active");
  });
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#sidebar-wrapper").toggleClass("active");
  });


});