$(document).ready(function(){
	$('.orderFood').click(function(){
		var date = moment().format('jD/jM/jYYYY');
		$.cookie('date', date);
		window.location = './order';
	});
	$('.reserveFood').click(function(){
		window.location = './reserve';
	});
});