var express = require('express'),
	app 	= express(),
	mongoose		= require('mongoose'),
 	bodyParser = require('body-parser'),
	db 		= require('./db'),
	cookieParser = require('cookie-parser'),
	session = require('express-session'),
	mongoStore = require('connect-mongo')(session),
/////////////////////|** multer for upload image
	multer  = require('multer'),
	storage = multer.diskStorage({
	  destination: function (req, file, cb) {
	    cb(null, './views/img/food')
	  },
	  filename: function (req, file, cb) {
	    cb(null, Date.now()+ '-'  + file.originalname)
	  }
	});
 
var upload = multer({ storage: storage });
/////////////////////|** multer for upload image

////////////////////|** Connect To Database
mongoose.connect("mongodb://localhost/foodsAutomation") ;
var dbSession = mongoose.connection ;
////////////////////|** Connect To Database

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/views'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(session({
    secret : "secret",
    resave : false,
    saveUninitialized : true ,
    store : new mongoStore({ mongooseConnection : dbSession})
})) ;




app.get('/', function(req, res, next) {
	db.getAllFood(function(foods, cms){
		res.render('./pages/index.ejs',{
			foods 	: foods,
			cms 	: cms
		});
	});
});

app.get('/page', function(req, res, next) {res.render('/pages/page.ejs')});

app.post('/addToCart', function(req, res, next) {
	//console.log(req.body)
});

app.post('/flwUpOrder', function(req, res, next) {
	var flwUpNum = req.body.flwUpNum;
	db.flwUpOrder(flwUpNum, function(cb) {
		res.send(cb);
	});
});

app.post('/searchFood', function(req, res, next) {
	var text = req.body.name;
	db.searchFood(text, function(foods) {
		res.json({
			foods 	: foods
		});
	});
});

app.get('/cart', function(req, res, next) {
	if(req.cookies.foodsId){
		res.render('./pages/cart.ejs');
	}else{res.redirect('/');}
});

app.get('/reserve', function(req, res, next) {
	if(req.cookies.foodsId){
		var split = req.cookies.foodsId.split(',');
		var ids = []
		for(var i=0 ; i <= split.length-1; i++){
		 	ids[i]= split[i] ;
		}
		db.getFoodsForCart(ids , function(foods){
			res.render('./pages/reserve.ejs', {
				foods : foods
			});
		});
	}else{res.redirect('/');}
});

app.get('/order', function(req, res, next) {
	if(req.cookies.foodsId){
		var split = req.cookies.foodsId.split(',');
		var ids = []
		for(var i=0 ; i <= split.length-1; i++){
		 	ids[i]= split[i] ;
		}
		var date = new Date();
		var cur_hour = date.getHours();
		db.getFoodsForCart(ids , function(foods){
			res.render('./pages/order.ejs', {
				hour  : cur_hour,
				foods : foods
			});
		});
	}else{res.redirect('/');}
});

app.get('/cart_login', function(req, res, next){
	if (req.cookies.foodsInfo) {
		if(req.session.auth){
			res.redirect('/pay');
		}else{
			res.render('./pages/cart-login.ejs');
		}
	}else{res.redirect('/');}
});

app.post('/loginUser', function(req, res, next){
	var user 	= req.body.user;
	var pass 	= req.body.pass;
	db.Authentication(user, pass , function(cb) {
		if(cb.status == "200"){
			req.session.auth = {username : user};
			res.send(cb);
		}else{
			req.session.auth = "";
			res.send("400");
		}
		
	});
});

app.post('/signUpUser', function(req, res, next) {
	var user 	= req.body.user,
		pass 	= req.body.pass,
		address = req.body.address,
		name 	= req.body.name;

	db.signUpUser(name, user, pass, address, function(cb) {
		if(cb.status == "200"){
			req.session.auth = {username : user};
			res.send(cb);
		}else{
			req.session.auth = ""; 
			res.send("500");
		}
						
	});

});

app.get('/pay', function(req, res, next) {
	console.log(req.cookies);
	if(req.session.auth){
		var username = req.session.auth.username;
		db.getAddressForPayPage(username, function(address, name) {
			res.render('./pages/user_pay.ejs', {
				address 	: address,
				tell		: username,
				name 		: name
			});
		});
	}else{ res.redirect('/'); }
});

app.post('/loginRestaurant', function(req, res, next) {
	var user = req.body.user;
	var pass = req.body.pass;
	db.authenticationRestaurant(user, pass, function(cb, username, id, name) {
		if(cb.status == "200"){
			req.session.isAdminUser = username;
			req.session.isAdminId 	 = id;
			req.session.isAdminName = name;
			res.send(cb);
		}else{res.send(cb);}
	});
});


app.get('/restaurant_login', function(req, res, next) {
	res.render('./pages/admin/login.ejs');
});

app.get('/admin', function(req, res, next) {
	if(req.session.isAdminUser){
		db.getOrdersForAdmin(function(cb){
			res.render('./pages/admin/index.ejs', {
				orders : cb
			});
		});
	}else{res.redirect('/')}
});

app.get('/products/add', function(req, res, next) {
	if(req.session.isAdminUser){
		res.render('./pages/admin/addProducts.ejs');
	}else{res.redirect('/')}
});

app.get('/products/', function(req, res, next) {
	if(req.session.isAdminUser){		
		var restUser = req.session.isAdminId;
		db.getFoodForAdminProducts(restUser, function(foods){
			res.render('./pages/admin/products.ejs', {
				foods : foods
			});
		});
	}else{res.redirect('/')}

});

app.post('/addFood', upload.single('file'), function (req, res) {
	if(req.session.isAdminUser){
		var newFood = {
			name 		: req.body.foodName,
			desc 		: req.body.foodDesc,
			pic  		: req.file.path.replace('views', '.'),
			rest_name	: req.session.isAdminName,
			rest_id	 	: req.session.isAdminId,
			typeOfFood 	: req.body.typeOfFood,
			priceRange 	: req.body.priceRange
		};

		db.addFoodForAdminAddProducts(newFood, function(cb){
			res.redirect('/admin');
		});
	}else{res.redirect('/')}
});

app.post('/deleteFood', function(req, res, next) {
	if(req.session.isAdminUser){	
		var id = req.body.id;
		db.deleteFood(id, function(cb) {
			res.send(cb);
		});
	}else{res.redirect('/')}
});

app.post('/modifyFood', function(req, res, next) {
	if(req.session.isAdminUser){	

		var id 		= req.body.id,
			price 	= req.body.price,
			stock 	= req.body.stock,
			off		= req.body.off,
			rest_id = req.session.isAdminId;
		db.modifyFood(id, price, stock, off, rest_id, function(cb){
			res.send(cb);
		});
	}else{res.redirect('/')}
});

app.post('/addComment', function(req, res, next) {
	var flwUpNumber = req.body.flwUpNumber,
		messageText = req.body.messageText;
	db.addComment(flwUpNumber, messageText, function(cb) {
		res.send(cb);
	});
});

app.get('/comments', function(req, res, next) {
	if(req.session.isAdminUser){	
		db.getCommentsForAdmin(function(cb) {
			res.render('./pages/admin/comments.ejs', {
				cms : cb
			})
		});
	}else{res.redirect('/')}

});

app.post('/approvCm', function(req, res, next) {
	if(req.session.isAdminUser){	
		var id = req.body.id;
		db.approvCm(id, function(cb) {
			res.send(cb);
		});
	}else{res.redirect('/')}

});

app.post('/deleteCm', function(req, res, next) {
	if(req.session.isAdminUser){	
		var id = req.body.id;
		db.deleteCm(id, function(cb) {
			res.send(cb);
		});
	}else{res.redirect('/')}	
});

app.post('/foodSended', function(req, res, next) {
	if(req.session.isAdminUser){
		var id = req.body.id;	
		db.foodSended(id, function(cb) {
			if(cb.status == 200){
				res.send(cb);
			}
		});
	}else{res.redirect('/')}
});


app.use(function (req, res, next) {
  res.status(404).redirect('/');
});

app.listen('80');
console.log("App running on port 80.");
module.exports = app;