var mongoose = require('mongoose');
var async = require('async');

var Schema = mongoose.Schema,
	ObjectId = Schema.ObjectId;
var restaurantSchema = new Schema({
	_id : String,
	name : String,
	username: String,
	password: String
});
var foodSchema = new Schema({
	name 		: String,
	desc 		: String,
	priceRange	: String,
	pic 		: String,
	typeOfFood 	: String,
	date 		: Date,
	restaurant 	: [{
		id 		: String,
		name    : String,
		price 	: Number,
		off 	: Number,
		stock 	: Number
	}]
});
var userSchema = new Schema({
	name 		: String,
	username	: String,
	password	: String,
	address		: String
});

var orderSchema = new Schema({
	id 			: String,
	status		: Boolean,
	date		: String,
	price 		: String,
	address 	: String,
	userid 		: String,
	rest_id 	: String,
	rest_name	: String
});

var commentSchema = new Schema({
	flwUpNumber : String,
	messageText : String,
	status		: Boolean,
	date 		: Date
});

foodSchema.index({name : 'text'});

var Comment 	= mongoose.model('Comment', commentSchema);
var User 		= mongoose.model('User', userSchema);
var Food 		= mongoose.model('Food', foodSchema);
var Restaurant 	= mongoose.model('Restaurant', restaurantSchema);
var Order 		= mongoose.model('Order', orderSchema);

module.exports = {
	mongooseConnection : function(){
		var db = mongoose.connection ;
	},
	getAllFood: function(cb){

		Food.find().sort({"restaurant.stock" : -1 }).exec(function(err , food){
			Comment.find({"status" : true}).sort({date : 'descending'}).limit(4).exec(function(err, cm) {
				cb(food, cm)
			});
		});
	},

	getFoodsForCart: function(ids, cb){
		var foods = [];
		async.each(ids, function(id, callb) {
			Food.findById({"_id" : id}, function(err, food){
				foods.push(food);
				callb();
			});
		}, function(err) {
			cb(foods);
		});
	},

	Authentication: function(user, pass, cb){
		User.findOne({username : user}, function(err , user){
			if(user != null){
				if(user.password == pass){
					cb({status : 200, userid: user.username});
				}else{cb({status : 400});}
			}else{cb({status : 400});}
		});
	},
	
	signUpUser: function(name, user, pass, address, cb) {
		var newUser = new User({
			name 	 : name,
			username : user,
			password : pass,
			address  : address
		});
		User.findOne({username : user}, function(err , user){

			if(user == null){
				newUser.save( function(err) {
					if (err) {throw err};
					cb({status : 200, userid: newUser.username});
				});
			}else{cb({status : 555} /* 555 Err : A user with this profie were found */);}
		});	
	},

	getAddressForPayPage : function(username, cb){
		User.findOne({username : username}, function(err, user) {
			cb(user.address, user.name);
		});
	},
	flwUpOrder: function(flwUpNum, cb){
		Order.findOne({id : flwUpNum}, function(err, order) {
			cb(order);
		});
	},

	searchFood: function(text, cb){
		Food.find({"name": new RegExp(text, "i")}, function(err, foods) {
			cb(foods)
		});
	},
//Restuarant ////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
	authenticationRestaurant:function(user, pass, cb) {
		Restaurant.findOne({username: user, password: pass}, function(err, restaurant){
			if(restaurant){
				cb({status: 200}, restaurant.username, restaurant._id, restaurant.name );
			}
			else{
				cb({status: 404})
			}

		});
	},
	getFoodForAdminProducts : function(restUser , cb){
		Food.find({"restaurant.id" : restUser},function(err, food){
			cb(food);
		});
	},

	addFoodForAdminAddProducts : function(newFood, cb){
		var newFood = new Food({
			name 		: newFood.name,
			desc 		: newFood.desc,
			priceRange	: newFood.priceRange,
			pic 		: newFood.pic,
			typeOfFood	: newFood.typeOfFood,
			date 		: new Date,
			restaurant 	: [{
				id 	: newFood.rest_id,
				name: newFood.rest_name,
				price: "",
				stock: ""
			}]			
		});
		newFood.save(function(err){
			if (err) {
			    console.log('Error Inserting New Data');
			    if (err.name == 'ValidationError') {
			        for (field in err.errors) {
			            console.log(err.errors[field].message); 
			        }
			    }
			}
			cb("ok");
		});
	},

	deleteFood: function(id, cb) {
		Food.find({_id : id}).remove().exec();
		cb({status: 200});
	},

	modifyFood: function(id, price, stock, off, rest_id, cb) {
		Food.findById({_id: id}, function(err, food){
			food.restaurant.forEach(function(rest){
				if(rest.id == rest_id){
					rest.price 	= price;
					rest.stock 	= stock;
					rest.off	= off;
					food.save();
					cb({status : 200});
				}
			});
		});
	},

	addComment: function(flwUpNumber, messageText, cb) {
		Order.findOne({"id": flwUpNumber}, function(err, order){
			if(order != null){
				Comment.find({"flwUpNumber":flwUpNumber}, function(err, cm){
					if(cm.length == 0){
						var cm = new Comment({
							flwUpNumber : flwUpNumber,
							messageText : messageText,
							date 		: new Date, 
							status 		: false
						});

						cm.save();
						cb({status : 200});
					}else{cb({status : 500});}
				});
			}else{
				cb({status : 400});
			}
		});
	}, 

	getOrdersForAdmin : function(cb) {
		Order.find({"status" : false}, function(err, order){
			cb(order);
		});
	},

	getCommentsForAdmin: function(cb) {
		Comment.find({}, null, {sort : {date : 'descending'}}, function(err, cm) {
			cb(cm);
		});
	},

	approvCm: function(id, cb) {
		Comment.findById({_id : id}, function(err, cm) {
			cm.status = true;
			cm.save();
			cb({status : 200});
		});
	},
	
	deleteCm: function(id, cb) {
		Comment.findById({_id : id}).remove().exec();
		cb({status: 200});
	},

	foodSended: function(id, cb) {
		Order.findOneAndUpdate({ "_id" : id}, {new: true}, function(err, order) {
			if(err) throw err;

			order.status = true;
			order.save();
			cb({status: "200"});
		});
	}
}
