$(document).ready(function(){
    var foodsInfo       = [],
        eachFood        = {},
        cookieArray     = $.cookie('foodsId');

    $('.foodsBoxCart').each(function() {
        var price = +($(this).find('.priceSpanTag span').text());
        var totalPrice      = +($('.totalPrice').text());
        $('.totalPrice').text(totalPrice+price);
    });

    $('.MMqty').click(function() {
        var numOfFood = +($(this).parent().find('.numOfFood').text());
        var price = +($(this).parent().parent().find('.priceSpanTag span').text());
        var totalPrice      = +($('.totalPrice').text());
        if (numOfFood > 1 ) {
            numOfFood--;
            $(this).parent().find('.numOfFood').text(numOfFood);
            $('.totalPrice').text(totalPrice-price);
        }
    });

    $('.PPqty').click(function() {
        var numOfFood = +($(this).parent().find('.numOfFood').text());
        var stock = +($(this).parent().parent().find('.qtyForEachRest span').text());
        var price = +($(this).parent().parent().find('.priceSpanTag span').text());
        var totalPrice      = +($('.totalPrice').text());
        if (numOfFood < stock){
            numOfFood++;
            $(this).parent().find('.numOfFood').text(numOfFood);
            $('.totalPrice').text(totalPrice + price);
        }
    });

    $('.deleteFoodFromCart').click(function(){
        var numOfFood = +($(this).parent().parent().find('.numOfFood').text());
        var price = +($(this).parent().parent().find('.priceSpanTag span').text());
        var totalPrice      = +($('.totalPrice').text());
    	$(this).parent().parent().parent().remove();
	 	if(totalPrice > 0){
            var totalPriceToString = totalPrice - (numOfFood*price); 
            $('.totalPrice').html(totalPriceToString);    	 		
	 	}
    });
    
    $('.contiuneBuy').click(function() {
        $('.foodsBoxCart').each(function() {
            eachFood.name       = $(this).find('.foodName').text();
            eachFood.restId     = $(this).find('#rest').attr('restId');
            eachFood.restName   = $(this).find('#rest').text().replace(/\s{2,}/g, ' ');
            eachFood.qty        = $(this).find('.numOfFood').text();
            foodsInfo.push(eachFood);
            eachFood = {};        
        }).promise().done(function() {
            var totalPrice = +($('.totalPrice').text()); 
            
            $.cookie('foodsInfo', foodsInfo);
            $.cookie('totalPrice', totalPrice);
            window.location = "/cart_login"
        });
    });
});	