$(document).ready(function(){
	//$.removeCookie('foodsId');
	$('.pics').slick({
	  	autoplay: true,
  		autoplaySpeed: 5000,
		mobileFirst:true,
        rtl: true,
	  	dots: true,
	  	infinite: true,
	  	slidesToShow: 1,
	  	speed:2000,
	  	adaptiveHeight: true
	});
    $('[data-toggle="tooltip"]').tooltip(); 
    	$(document).scroll(function(e) {
	    	if($( window ).width() > 991) {
		    	if($(document).scrollTop() > 803) {
		    		//$(".fixSidebar").css({"position":"fixed","top":"0"});
		    	}if($(document).scrollTop() < 760){
		    		$(".fixSidebar").css("position","inherit");
		    	}				    	
		    }
	    });
		$(document).scroll(function(e) {
			if($( window ).width() < 991) {
    			$(".fixSidebar").css("position","inherit");
    		}
    		if($(window).width() < 602) {
    			if($(document).scrollTop() > 280) {
					$(".menuMobile").css("display","block");
    			}
    		}
    		if($(window).width() > 602) {$(".menuMobile").css("display","none");}
	
		});
	
	$('.checkOutFood img').click(function(){
		alert('as');
	});


	var cartCounter = 0;
	var foodsInCart=[];
	$('.bottomFoodbox .foodBoxChecker').click(function() {
		$(this).css({"display":"none"});
		var foodName = $(this).parent().parent().find('.headerBox span').html();
		if(cartCounter == 0){
			$('.checkOutFood').html("");
		}
		$(this).parent().parent().find('.selectedFood').css("display","block");
		cartCounter = cartCounter + 1;
		$('.badge').html(cartCounter);
		$('.checkOutFood').append("<div class='removeFoodIndex'><span>"+foodName+"</span></div>");
		var id = $(this).attr('name');
		$(this).removeAttr("name");
		foodsInCart.push(id);
		$.cookie('foodsId', foodsInCart);
		$(this).attr('src', '');
	});


	$('.submitComment').click(function() {
		var flwUpNumber = $('.flwUpNumber').val(),
			messageText = $('.messageText').val();
		if(flwUpNumber.length != 0 && messageText.length != 0) {
			$.post('/addComment', {flwUpNumber : flwUpNumber, messageText : messageText}, function(data) {
				if (data.status == 200) {
					$('#sendComment').html('<div class="alert alert-success fade in" role="alert">دیدگاه شما با موفقیت ثبت شد و پس از تایید مدیران نمایش داده میشود .</div>');
				}
				if (data.status == 400) {
					$('.flwUpNumber').css("border","1px solid red");
					$('.modal-body p').html("شماره ی پیگیری نامعتبر است .");
					$('#myModal').modal('show');
	
				}
				if (data.status == 500) {
					$('.flwUpNumber').css("border","1px solid red");
					$('.modal-body p').html("دیدگاهی با این شماره سفارش از پیش ثبت شده است .");
					$('#myModal').modal('show');
	
				}
			});
		}else{
			$('.modal-body p').html("فیلد های شماره پیگیری و متن دیدگاه خود را تکمیل کنید .");
			$('#myModal').modal('show');

		}
	});
	$('.doneBuy').click(function() {
		if(cartCounter > 0){
			$('.cartItem').css({"height":"90px"});
			$('.optionCart .col-md-12').delay(200).queue(function(next) {
				$(this).css({"display":"block"});
				$(this).css({"opacity":"1"});
				next();
			});
			$('.glyphicon-menu-left').css({"transition:":"all 500ms","-ms-transform":"rotate(-90deg)", "-webkit-transform":"rotate(-90deg)", "transform":"rotate(-90deg)"})
		}else{
			$('.modal-body p').html("لطفا ابتدا محصولی را به سبد خرید, اضاف کنید .");
			$('#myModal').modal('show');
		}

	});

	$('.orderFood').click(function() {
		if (cartCounter > 0 ) {
			window.location = '/order';
		}else{
			$('.modal-body p').html("لطفا ابتدا محصولی را به سبد خرید, اضاف کنید .");
			$('#myModal').modal('show');
		}
	});
	$('.reserveFood').click(function() {
		if (cartCounter > 0 ) {
			window.location = '/reserve';
		}else{
			$('.modal-body p').html("لطفا ابتدا محصولی را به سبد خرید, اضاف کنید .");
			$('#myModal').modal('show');
		}
	});

	$('.navigateMenuMobile').click(function(){
		if (cartCounter > 0 ) {
			$('.menuMobile .checkOutFood').toggleClass('showCheckOutFoodMobile');
		}else{
			$('.modal-body p').html("لطفا ابتدا محصولی را به سبد خرید, اضاف کنید .");
			$('#myModal').modal('show');
		}
		
	});
});