$(document).ready(function() {
	$('.subCm').click(function() {
		var btn = $(this);
		var id = $(this).attr("cmId");
		$.post('/approvCm', {id : id}, function(data) {
			$(btn).removeClass("subCm");
			$(btn).attr("value", "تایید شده");
		});
	});
	$('.dlteCm').click(function() {
		var btn = $(this);
		var id = $(this).attr("cmId");
		$.post('/deleteCm', {id : id}, function(data) {
			location.reload();
		});
	});
});