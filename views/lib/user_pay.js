$(document).ready(function() {
    $('input:radio[name=optradio]').change(function() {
        if (this.value == '0') {
        	$('.customerAddress').attr('readonly' , '');
            $('.alert-info').css('display', 'none');
            $('.col-md-5').css('height', '410px');       
        }
        else if (this.value == '1') {
            $('.modal-body p').html("لطفا از صحیح بودن آدرس اطمینان حاصل کنید . و در صورت نیاز اقدام به ویرایش آن کنید .");
            $('#myModal').modal('show');
            $('.customerAddress').removeAttr('readonly');
            $('.alert-info').css('display', 'block');
            $('.col-md-5').css('height', '548px');
        }
    });
    var date = moment().format('jYYYY/jM/jD');
    $('.dateFactor').html(date);
    $.cookie('date', date);

    var totalPrice = $.cookie('totalPrice');
    $('.totalPriceToPay').html(totalPrice + " تومان ");
});